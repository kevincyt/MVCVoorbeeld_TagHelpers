﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace LesVoorbeeldTagHelpers.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("button")]
    public class ButtonExtensionTagHelper : TagHelper
    {

        [HtmlAttributeName("asp-is-submit")]
        public bool IsSubmit { get; set; }

        [HtmlAttributeName("asp-show-text")]
        public string ButtonText { get; set; }

        [HtmlAttributeName("asp-show-as")]
        public string ButtonType { get; set; }


        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "button";
            if (IsSubmit)
            {
                output.Attributes.Add("type", "submit");
            }
            else
            {
                output.Attributes.Add("type", "button");
            }
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Content.SetContent(ButtonText);
            if (ButtonType != null && ButtonType.Equals("link"))
            {
                // Appending the "btn-link" class to the button.
                TagHelperAttribute tagAttr = output.Attributes.FirstOrDefault((attr) =>
                {
                    return attr.Name == "class";
                });
                string ammended = tagAttr.Value.ToString() + " btn-link";
                output.Attributes.Remove(tagAttr);
                output.Attributes.Add("class", ammended);
            }
        }
    }
}
